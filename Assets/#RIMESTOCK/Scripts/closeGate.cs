﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class closeGate : MonoBehaviour 
{
    public GameObject leftDoor, rightDoor;

    private bool readyToClose = false;

    private bool isVisible = false;


    //if player is left in this rectangle and don't see an object - close gate. Elsether - don't close the gate
    public Rect areaRect;

    private bool closed = false;
	
	void FixedUpdate () 
    {
	    if(!closed && readyToClose && !isVisible)
        {
            leftDoor.GetComponent<Door>().Close();
            rightDoor.GetComponent<Door>().Close();
            GetComponent<AudioSource>().Play();
            Destroy(gameObject, 5);
            closed = true;
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
            readyToClose = true;
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player" && !areaRect.Contains(collider.transform.position))
            readyToClose = false;
    }

    void OnBecameInvisible()
    {
        isVisible = false;
    }

    void OnBecameVisible()
    {
        isVisible = true;
    }
}
