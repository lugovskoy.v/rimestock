﻿using UnityEngine;
using System.Collections;
using Rimestock;


[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(ParticleSystem))]
public class SpellcastControl : MonoBehaviour 
{
    public Object LMB, RMB;
    public float LMB_castTime= 0.5f, RMB_castTime = 1.5f;
    public float LMB_cast = 0, RMB_cast = 0;//how much seconds are casting spells

    private ParticleSystem ps;

    private GameObject player;
    private CharacterController playerCC;

    public LayerMask blinkLayerMask;
    public float blinkDistance = 10.0f;
    public float blinkCoolDown = 1.0f;

    private float blinkCD = 0.0f;

    public AudioClip blinkAudio;

	void Start () 
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerCC = player.GetComponent<CharacterController>();
        ps = GetComponent<ParticleSystem>();
	}
	
	void Update () 
    {
        if (blinkCD > 0)
            blinkCD -= Time.deltaTime;
        else if(Input.GetKeyDown(KeyCode.Space))//~10m blink
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            if (Mathf.Abs(horizontal) >= Mathf.Abs(vertical))
            {
                vertical = 0;
                if (horizontal > 0)
                    horizontal = 1.0f;
                else if (horizontal < 0)
                    horizontal = -1.0f;
            }
            else
            {
                horizontal = 0;
                if (vertical > 0)
                    vertical = 1.0f;
                else if (vertical < 0)
                    vertical = -1.0f;
            }
            var directionVector = new Vector3(horizontal, 0, vertical);
            if (directionVector != Vector3.zero)
            {
                RaycastHit hit;
                if (Physics.SphereCast(new Ray(player.transform.position, directionVector), 2.0f, out hit, blinkDistance, blinkLayerMask))//character detected
                {
                    LayerMask msk = ~blinkLayerMask.value;//detect any obstacles
                    float distance = blinkDistance;
                    if (Physics.SphereCast(new Ray(player.transform.position, directionVector), 2.0f, out hit, blinkDistance, msk))
                    {
                        distance = Vector3.Distance(player.transform.position, hit.point) - 1.0f;
                    }

                    //Debug.Log(distance +" till " + hit.transform.name);
                    if (distance > 0)
                    {
                        //if (distance > blinkDistance)
                        //    distance = blinkDistance;
                        GetComponent<AudioSource>().PlayOneShot(blinkAudio);
                        CameraFade.StartAlphaFade(new Color(0, 0, 0), false, 1f);
                        player.transform.Translate(directionVector * distance);
                        //playerCC.SimpleMove(directionVector * distance);
                        blinkCD = blinkCoolDown;
                    }
                }
                else
                {
                    GetComponent<AudioSource>().PlayOneShot(blinkAudio);
                    CameraFade.StartAlphaFade(new Color(0, 0, 0), false, 1f);
                    //Vector3 direction = Vector3.Cross(Camera.mainCamera.transform.forward, directionVector);
                    Quaternion q = Camera.main.transform.rotation;
                    q.x = 0;
                    //q.y = 0;
                    playerCC.Move((q*directionVector) * blinkDistance);
                //playerCC.SimpleMove(directionVector * blinkDistance);
                    //player.transform.Translate(directionVector * blinkDistance);
                    blinkCD = blinkCoolDown;
                }
            }
        }

        if (LMB_cast == 0 && RMB_cast == 0)//nothing is casting
        {
            if (Input.GetMouseButtonDown(0))
            {
                LMB_cast += Time.deltaTime;
                startCasting();
            }
            /*if (Input.GetMouseButtonDown(1))
            {
                RMB_cast += Time.deltaTime;
                startCasting();
            }*/
        }
        else if (LMB_cast != 0 && RMB_cast != 0)//both are casting?
        {
            //we have no LMB+RMB spell yet
            LMB_cast = 0;
            RMB_cast = 0;
            stopCasting();
        }
        else if (LMB_cast != 0)//LMB casting
        {
            if (Input.GetMouseButtonUp(0))//button released
            {
                if (LMB_cast >= LMB_castTime)//Launch spell!
                {
                    Quaternion dir = Quaternion.LookRotation(Camera.main.transform.forward, Camera.main.transform.position - transform.position);
                    GameObject go = (GameObject)Instantiate(LMB, transform.position, dir);
                    EffectSettings es = go.GetComponent<EffectSettings>();
                    RaycastHit hit;
                    //
                    es.Target = new GameObject();
                    Destroy(es.Target, 20);
                    //
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2)), out hit))
                    {      
                        es.Target.transform.position = hit.point;  
                    }
                }
                stopCasting();
                LMB_cast = 0;
            }
            else if (Input.GetMouseButton(0))//if button is pressed still
            {
                LMB_cast += Time.deltaTime;
            }
            else
            {
                stopCasting();
                LMB_cast = 0;
            }
        }
        else if (RMB_cast != 0)//RMB casting
        {

        }
	}


    private void startCasting()
    {
        GetComponent<AudioSource>().Play();
        ps.Play();
    }
    private void stopCasting()
    {
        GetComponent<AudioSource>().Stop();
        ps.Stop();
    }
}
