﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
    public Object spawn;

    public bool isSpawning = false;
    public float delay = 5;
    private bool isReadyToSpawn = true;

    public LayerMask mask;

	void Update () 
    {
        if (isReadyToSpawn && isSpawning)
        {
            if (Physics.OverlapSphere(transform.position, 0.5f).Length == 0)
            {
                Instantiate(spawn, transform.position, Quaternion.identity);
                isReadyToSpawn = false;
                StartCoroutine(Spawn(delay));
            }
        }
	}

    IEnumerator Spawn(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        
        isReadyToSpawn = true;
    }
}
