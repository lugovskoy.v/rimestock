﻿using UnityEngine;
using System.Collections;

public class PopupDamageInfo : MonoBehaviour 
{
    public float lifetime = 2.0f, curLifetime;
    private TextMesh text;

    private static Transform player = null;

    void Awake()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player").transform;
        text = GetComponent<TextMesh>();
    }

    void Start () 
    {
        curLifetime = lifetime;
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.LookAt(player);
        transform.Rotate(Vector3.up, 180.0f);
        if (curLifetime > 0)
        {
            curLifetime -= Time.deltaTime;
            transform.position += 2 *Vector3.up * Time.deltaTime;
        }
        else
            DestroyImmediate(gameObject);
	}

    public void setInfo(string s, Color color)
    {
        text.text = s;
        text.color = color;
    }
}
