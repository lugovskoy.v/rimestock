﻿using UnityEngine;
using System.Collections;
using System;

public enum DamageSource
{
    FALLING,
    PHYSICAL,
    FIRE
}

[Serializable]
public class Damage
{
    public DamageSource source;
    public float value;

    public Damage(float value, DamageSource source)
    {
        this.value = value;
        this.source = source;
    }
}
