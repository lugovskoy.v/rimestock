﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public Vector3 closeRotation;

    public void Close()
    {
        transform.localRotation = Quaternion.Euler(closeRotation);
    }
}
