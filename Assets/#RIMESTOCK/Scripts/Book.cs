﻿using UnityEngine;
using System.Collections;

public class Book : MonoBehaviour 
{
    public Transform player;
    public GameObject textObject;

    public Object pickUpEffect;

    public float textShowDistance = 2;

    public Spawner spawner;

    bool bookUsed = false;
   
    //private TextMesh textMesh;

	// Use this for initialization
	void Start () 
    {
        textObject.SetActive(false);
        //textMesh = textObject.GetComponent<TextMesh>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Camera.main.transform.Find("FireSpells").GetComponent<SpellcastControl>().enabled = false;
	}

    private Ray ray;
    private RaycastHit hit;
	// Update is called once per frame
	void Update () 
    {
        if (player != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 50))
            {
                if (hit.transform.tag == "Interactive")
                {
                    Vector3 heading = transform.position - player.position;
                    if (heading.sqrMagnitude <= textShowDistance * textShowDistance)
                        textObject.SetActive(true);
                    else
                        textObject.SetActive(false);
                }
                else
                    textObject.SetActive(false);
            }
            else
                textObject.SetActive(false);
        }

        if (textObject.activeSelf)
        {
            textObject.transform.LookAt(player.position + (Vector3.up * 0.5f));
            textObject.transform.Rotate(Vector3.up, 180);

            if (Input.GetKeyDown(KeyCode.E) && !bookUsed)
            {
                BookPickedUp();
                bookUsed = true;
            }
        }
	}

    void OnMouseEnter()
    {
        Vector3 heading = transform.position - player.position;
        if (heading.sqrMagnitude <= textShowDistance * textShowDistance)
        {
            textObject.SetActive(true);
        }
    }

    void OnMouseExit()
    {
        textObject.SetActive(false);
    }


    void BookPickedUp()
    {
        GameObject efx = (GameObject)Instantiate(pickUpEffect, transform.position + new Vector3(-0.5f, 0, 0), Quaternion.identity);
        Destroy(efx, 5);
        spawner.isSpawning = true;
        StartCoroutine(PushPlayer(1));
    }

    IEnumerator PushPlayer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Vector3 heading = player.position - transform.position;
        player.SendMessage("AddImpact", heading.normalized * 32);// 32 - push power
        player.SendMessage("PlayBattleMusic");
        Camera.main.transform.Find("FireSpells").GetComponent<SpellcastControl>().enabled = true;
        Destroy(gameObject);
    }

   
}
