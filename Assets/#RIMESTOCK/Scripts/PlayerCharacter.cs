﻿using UnityEngine;
using System.Collections;

namespace Rimestock
{
    [RequireComponent(typeof(FPSInputController))]
    [RequireComponent(typeof(CharacterMotor))]
    [RequireComponent(typeof(CharacterController))]
    public class PlayerCharacter : MonoBehaviour
    {
        private const float fallingSave = 1.5f, fallingDeath = 15.0f, fallingDamageModifier = 5.0f;
        private float fallingStartY = 0;

        public Health health;
        public Texture2D healthBarTexture;

        private Vector3 impact = Vector3.zero;
        private float mass = 2, impactVanishingSpeed = 5; // player's mass. For impact calculation

        private CharacterController cc;

        private bool heartbeatPlaying = false;

        void Awake()
        {
            CameraFade.StartAlphaFade(Color.black, true, 2f, 0);
        }

        void Start()
        {
            health.restore();
            cc = GetComponent<CharacterController>();
        }

        void Update()
        {
            if (health.isDead)
            {
                //gameObject.SetActive(false);
                enabled = false;
                GetComponent<CharacterMotor>().canControl = false;
                GetComponent<MouseLook>().enabled = false;
                CameraFade.StartAlphaFade(Color.white, false, 4f, 0, () => { Application.LoadLevel(0); });
            }


            //Calculate impact momentum
            if (impact.magnitude > 0.2f)
            { // if momentum > 0.2...
                cc.Move(impact * Time.deltaTime); // move character
            }
            // impact vanishes to zero over time
            impact = Vector3.Lerp(impact, Vector3.zero, impactVanishingSpeed * Time.deltaTime);
        }

        public void ApplyDamage(Damage damage)
        {
            if (damage.source == DamageSource.FALLING)
                Debug.Log("falling damage received: " + damage.value);
            health.applyDamage(damage.value);
            if (!heartbeatPlaying && health.HP / health.maxHP < 0.3f)
            {
                GetComponent<AudioSource>().PlayOneShot(hearthbeatAudioClip, 1.0f);
                StartCoroutine(StartHeartbeat(4));
                heartbeatPlaying = true;
            }
        }

        public AudioClip hearthbeatAudioClip;
        IEnumerator StartHeartbeat(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            heartbeatPlaying = false;
        }

        public void Kill(DamageSource source)
        {
            ApplyDamage(new Damage(health.HP, source));
            if (source == DamageSource.FALLING)
                Debug.Log("Your character fell to death");
        }

        public void OnFall()
        {
            fallingStartY = transform.position.y;
        }

        public void OnLand()
        {
            float fallingDistance = fallingStartY - transform.position.y;
            if (fallingDistance > fallingSave)
            {
                if (fallingDistance >= fallingDeath)
                    Kill(DamageSource.FALLING);
                else
                    ApplyDamage(new Damage(fallingDistance * fallingDamageModifier, DamageSource.FALLING));
            }
        }

        public void AddImpact(Vector3 force)
        {
            var dir = force.normalized;
            dir.y += 0.5f; // add some velocity upwards - it's cooler this way
            impact += dir.normalized * force.magnitude / mass;
        }


        public AudioClip battleMusic;
        public void PlayBattleMusic()
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().clip = battleMusic;
            GetComponent<AudioSource>().volume = 0.2f;
            GetComponent<AudioSource>().Play();
            GetComponent<AudioSource>().loop = true;
        }

        void OnGUI()
        {
            GUIHealth();
        }


        public Rect healthBarPosition;
        void GUIHealth()
        {
            GUI.color = Color.black;
            GUI.DrawTexture(healthBarPosition, healthBarTexture);
            GUI.color = Color.white;
            Rect r = healthBarPosition;
            r.width *= health.HP / health.maxHP;
            GUI.DrawTextureWithTexCoords(r, healthBarTexture, new Rect(0, 0, health.HP / health.maxHP, 1));
        }
    }

}