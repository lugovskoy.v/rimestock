﻿using UnityEngine;
using System.Collections;
using System;

namespace Rimestock
{
    [Serializable]
    public class Health
    {
        public float HP, maxHP;
        public bool isDead = false;

        public void restore()
        {
            if(!isDead)
                HP = maxHP;
        }

        public void applyDamage(float damage)
        {
            HP -= damage;
            if (HP <= 0)
            {
                HP = 0;
                isDead = true;
            }
        }

    }
}
