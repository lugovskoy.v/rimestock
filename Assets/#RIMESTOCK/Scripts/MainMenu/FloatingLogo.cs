﻿using UnityEngine;
using System.Collections;

public class FloatingLogo : MonoBehaviour 
{
    public float currentX, currentY, targetX;
    public float speed = 3.0f, fadeSpeed = 0.1f;

    public Texture2D logo;

    private float alpha;
    private float labelAlpha = 0.75f, labelSpeed = 0.1f;
    private bool labelGrossing = true;

    public bool fadeIn = false, fadeOut = false;
	// Use this for initialization
	void Start () 
    {
        //logo = new GUITexture();
        randomizePosition();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            CameraFade.StartAlphaFade(Color.black, false, 2f, 1f, () => { Application.LoadLevel(1); });
        }

        //fade label
        if (labelGrossing)
        {
            if (1 - labelAlpha < labelSpeed * Time.deltaTime)
            {
                labelAlpha = 1;
                labelGrossing = false;
            }
            else
                labelAlpha += labelSpeed * Time.deltaTime;
        }
        else
        {
            if (labelAlpha - 0.75f < labelSpeed * Time.deltaTime)
            {
                labelAlpha = 0.75f;
                labelGrossing = true;
            }
            else
                labelAlpha -= labelSpeed * Time.deltaTime;
        }

        if (fadeIn)
        {
            if (alpha > fadeSpeed * Time.deltaTime)
                alpha -= fadeSpeed * Time.deltaTime;
            else
            {
                fadeIn = false;
                randomizePosition();
            }
            return;
        }
        else if (fadeOut)
        {
//            Debug.Log(alpha);
            if (alpha < 1 - fadeSpeed * Time.deltaTime)
                alpha += fadeSpeed * Time.deltaTime;
            else
                fadeOut = false;
            return;
        }

        if (currentX != targetX)
        {
            if (currentX > targetX)
            {
                if (currentX - targetX <= speed * Time.deltaTime)
                {
                    currentX = targetX;
                    fadeIn = true;
                }
                else
                {
                    currentX -= speed * Time.deltaTime;
                }
            }
            else
            {
                if (targetX - currentX <= speed * Time.deltaTime)
                {
                    currentX = targetX;
                    fadeIn = true;
                }
                else
                {
                    currentX += speed*Time.deltaTime;
                }
            }
        }
	}

    private void randomizePosition()
    {
        currentY = Random.Range(0, Screen.height - 100);
        if (Random.Range(0, 2) < 1)
        {//spawn on left
            currentX = Random.Range(0, Screen.width - 683);
            targetX = Screen.width - 683;
        }
        else
        {//spawn on right
            targetX = 0;
            currentX = Random.Range(0, Screen.width - 683);
        }

        alpha = 0;
        fadeOut = true;
    }

    void OnGUI()
    {        
        GUI.color = new Color(1, 1, 1, alpha);
        GUI.DrawTexture(new Rect(currentX, currentY, 683, 100), logo);
        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUI.color = Color.red;
        GUILayout.FlexibleSpace();
        GUILayout.Label("Press enter to start");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    
}
