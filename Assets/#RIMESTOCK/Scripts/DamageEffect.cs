﻿using UnityEngine;
using System.Collections;

public class DamageEffect : MonoBehaviour 
{
    public Damage damage;
    public float destroyAfter = 3;
    public LayerMask layerMask;
    public float collisionRadius = 0.5f;

    // Use this for initialization
	void Start () 
    {
        Destroy(gameObject, destroyAfter);
        foreach (Collider c in Physics.OverlapSphere(transform.position, collisionRadius, layerMask))
        {
            c.transform.SendMessage("ApplyDamage", damage);
        }
	}
	
}
