﻿using UnityEngine;
using System.Collections;

public class MouseOverChangeColor : MonoBehaviour 
{
    public bool isHover;
    public Color hoverColor;
    private Color commonColor;

    private SpriteRenderer sRenderer;

	// Use this for initialization
	void Start () 
    {
        sRenderer = GetComponent<SpriteRenderer>();
        isHover = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnMouseEnter()
    {
        isHover = true;
        commonColor = sRenderer.color;
        sRenderer.color = hoverColor;
    }

    void OnMouseExit()
    {
        isHover = false;
        sRenderer.color = commonColor;
    }
}
