﻿using UnityEngine;
using System.Collections;

public class Spells : MonoBehaviour
{
    public GameObject Shield;
    public GameObject Shield1;
    public GameObject Plane;

    public Transform effect;
    public int damage = 100;

    Vector3 startV = new Vector3();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.anyKeyDown)
        {
            startV = Camera.main.transform.position;
            GameObject go;

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                go = (GameObject)(Instantiate(Resources.Load("PurpleFireball1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                GameObject go1, go2;
                Vector3 goV1 = new Vector3(10, 2, 0);
                Vector3 goV2 = new Vector3(10, 2, 10);
                go = (GameObject)(Instantiate(Resources.Load("Firebolt1"), startV, Quaternion.identity));
                go1 = (GameObject)(Instantiate(Resources.Load("Firebolt1"), startV + goV1, Quaternion.identity));
                go2 = (GameObject)(Instantiate(Resources.Load("Firebolt1"), startV + goV2, Quaternion.identity));

                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);

                EffectSettings efx1 = go1.GetComponent<EffectSettings>();
                efx1.Target = Shield;
                Destroy(go1, 20);

                EffectSettings efx2 = go2.GetComponent<EffectSettings>();
                efx2.Target = Shield;
                Destroy(go2, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                go = (GameObject)(Instantiate(Resources.Load("Firebolt1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                go = (GameObject)(Instantiate(Resources.Load("Fireball2_V"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);


            }

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                Vector3 goV1 = new Vector3(10, 0, 0);
                go = (GameObject)(Instantiate(Resources.Load("Fireball_Mega_with_Fire"), startV + goV1, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                go = (GameObject)(Instantiate(Resources.Load("PurpleFireball1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                go = (GameObject)(Instantiate(Resources.Load("FrostBolt1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            //if (Input.GetKeyDown(KeyCode.Alpha8))
            //{
            //    go = (GameObject)(Instantiate(Resources.Load("FrostMeteor1"), startV, Quaternion.identity));
            //    EffectSettings efx = go.GetComponent<EffectSettings>();
            //    efx.Target = Shield;
            //    Destroy(go, 20);
            //}
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                go = (GameObject)(Instantiate(Resources.Load("FireMeteor1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                go = (GameObject)(Instantiate(Resources.Load("FireMeteor1"), startV, Quaternion.identity));
                EffectSettings efx = go.GetComponent<EffectSettings>();
                efx.Target = Shield;
                Destroy(go, 20);
            }


        }

        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            Vector3 shift = new Vector3(Random.Range(-2, 2), Random.Range(-1, 1), Random.Range(-2, 2));
            
            
            startV = Plane.transform.position + shift;
            startV.y += 15;

            GameObject go;

            go = (GameObject)(Instantiate(Resources.Load("FrostMeteor1"), startV, Quaternion.identity));
            ProjectileCollisionBehaviour behaviour =  go.GetComponentsInChildren<ProjectileCollisionBehaviour>()[0];
           // behaviour.endPointShift = shift;

            EffectSettings efx = go.GetComponent<EffectSettings>();
            efx.Target = Plane;
            Destroy(go, 20);

            //efx.Target.transform.position.x = Plane.transform.position.x;
        }
     
    }
}