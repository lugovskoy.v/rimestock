// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//Based on BuildIn Shaders
//Written 2012 by EyecyArt

Shader "Hidden/NightvisionEffect" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Mask ("Overlay", 2D) = "black" {}
    _Color ("Main Color", Color) = (0.5, 0.5, 0.5, 0)
	_Contrast ("Contrast", Range (0, 2)) = 1
	_Brightness ("Brightness", Range (0, 2)) = 1
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off Fog { Mode off }

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct v2f { 
	float4 pos	: POSITION;
	float2 uv	: TEXCOORD0;
	float2 uvo	: TEXCOORD1; // overlay
}; 

uniform sampler2D _MainTex;
sampler2D _Mask;
fixed4 _Color;
uniform fixed2 _OverlayScale;
uniform fixed2 _OverlaySpeed;
half _Contrast;
half _Brightness;

v2f vert (appdata_img v)
{
	v2f o;
	o.pos = UnityObjectToClipPos (v.vertex);
	o.uv = MultiplyUV (UNITY_MATRIX_TEXTURE0, v.texcoord);
	o.uvo = v.texcoord.xy * _OverlayScale.xy + (_Time*_OverlaySpeed.xy);
	return o;
}

fixed4 frag (v2f i) : COLOR
{
	fixed4 col = tex2D(_MainTex, i.uv);
	fixed4 mask = tex2D(_Mask, i.uvo);
	
	col.rgb = (col.r+col.b+col.g)*0.334;
	//col.rgb += col.a;
	col.rgb = (col.rgb+_Brightness)*2;
	col.rgb *= 1-((1-col.rgb) * _Contrast);
	col.rgb *= (1-((1-col.a) * _Color.a))*(mask+1); 

	//col.rgb = (col.r+col.b+col.g+col.a)*0.25;
	col *= _Color;

	return col;
}
ENDCG
	}
}

Fallback off

}

//Written 2012 by EyecyArt