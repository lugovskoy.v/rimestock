/* 2012 by Kevin Scheitler - EyecyStudio
 * contact@eyecystudio.de
 */
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
[AddComponentMenu("Image Effects/Advanced UnsharpMask")]
public class AdvancedUnsharpMask : MonoBehaviour
{
    public float Pixel = 0.5f;
    public float Amount = 1.0f;
    public float SecondAmount = 1.0f;
    public float Threshold = 0.1f;
    public float Contrast = 1.0f;
    public Shader shader;
	private Material m_Material;
	
	protected void Start ()
	{
		if( shader == null)
		{
			Debug.Log( "Noise shaders are not set up! Disabling noise effect." );
			enabled = false;
		}
		else
		{
            if (!shader.isSupported)
            {
                Debug.Log("Shader is not supported!");
                enabled = false;
            }
		}
	}

    void OnEnable()
    {
        GetComponent<Camera>().depthTextureMode |= DepthTextureMode.DepthNormals;
    }

	protected Material material {
		get {
			if( m_Material == null ) {
				m_Material = new Material( shader );
				m_Material.hideFlags = HideFlags.HideAndDontSave;
			}
			return m_Material;
		}
	}

	// Called by the camera to apply the image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{

		Material mat = material;

        mat.SetFloat("_Pixel", Pixel);
        mat.SetFloat("_Amount", Amount);
        mat.SetFloat("_Amount2", SecondAmount);
        mat.SetFloat("_Threshold", Threshold);
        mat.SetFloat("_Contrast", Contrast);

        mat.SetVector("_ScreenSize", new Vector2(Screen.width,Screen.height));

		Graphics.Blit (source, destination, mat);
	}

    internal void AddAmount()
    {
        Amount = (Amount+0.5f) % 3.5f;
        if (Amount == 0)
            this.enabled = false;
        else this.enabled = true;
    }
}

// 2012 by Kevin Scheitler - EyecyStudio