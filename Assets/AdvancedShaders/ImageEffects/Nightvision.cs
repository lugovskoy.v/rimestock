/* 2012 by Kevin Scheitler - EyecyStudio
 * contact@eyecystudio.de
 */
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
[AddComponentMenu("Image Effects/Nightvision")]
public class Nightvision : MonoBehaviour
{
    public Color VisionColor = new Color(55f / 255, 188f / 255, 58f / 255, 0f / 255);
    public Texture2D Overlay;
    public float OverlayScale = 1, Contrast = 1.72f, Brightness = 0.3f;
    public Vector2 OverlaySpeed = new Vector2(10,-1f);
	public Shader shader;
	private Material m_Material;
	
	protected void Start ()
	{

		if( shader == null)
		{
			Debug.Log( "Noise shaders are not set up! Disabling noise effect." );
			enabled = false;
		}
		else
		{
            if (!shader.isSupported)
            {
                Debug.Log("Shader is not supported!");
                enabled = false;
            }
		}
	}
	
	protected Material material {
		get {
			if( m_Material == null ) {
				m_Material = new Material( shader );
				m_Material.hideFlags = HideFlags.HideAndDontSave;
			}
			return m_Material;
		}
	}

	// Called by the camera to apply the image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{

		Material mat = material;

        if (Overlay != null)
        {
            mat.SetTexture("_Mask", Overlay);

            Vector2 scale = OverlayScale * new Vector2(
                (float)Screen.width / (float)Overlay.width,
                (float)Screen.height / (float)Overlay.height);

            mat.SetVector("_OverlaySpeed", OverlaySpeed);
            mat.SetVector("_OverlayScale", scale);
        }
        else
            mat.SetTexture("_Mask", null);

        mat.SetColor("_Color", VisionColor);
        mat.SetFloat("_Contrast", Contrast);
        mat.SetFloat("_Brightness", Brightness);

		Graphics.Blit (source, destination, mat);
	}
}

// 2012 by Kevin Scheitler - EyecyStudio